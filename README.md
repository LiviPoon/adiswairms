AWI - An exploration into cheaper methods in automatic farming
Jul 2018 – Apr 2019
Creating something that is simple yet sophisticated, big, yet reliable, 
is really difficult. In this project I strive to make an Automatic Irrigation 
System that the general population will be able to make and use on their own. 
The goal of the project is to make a drip irrigation system that uses multiple 
soil moisture sensors to monitor soil moisture within a planters box, or monitor 
many planter boxes. The data then would be transferred back into the device, 
which would calculate if the drip irrigation pump needs to be activated. While 
that is going on, the data is being transferred to the computer which can be 
monitored at any given time. The purpose of the project was to keep the price 
down, but to maximize crop growth, and monitor the weather, soil moisture, and 
water consumption.